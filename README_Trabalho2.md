# Trabalho2

Segundo trabalho de ICCA
 O trabalho consiste num pseudo terminal em que é carregado 8 comandos diferentes de um arquivo tipo texto. O algoritmo recebe via teclado o comando desejado e coloca essa string num vetor de caracteres. O comando é comparado com um dos comandos da lista interna do arquivo de entrada através da similaridade do cosseno. Quanto mais próximo de 1 o calculo do cosseno estiver, mais próximo está o comando de um dos comandos da lista. Para cada comando digitado, se o usuário optar que é o que deseja, um contador incrementa correspondendo ao quantas vezes foi usado, além de gravar tal estatística no arquivo de saída.

 A idéia desenvolvida para o algoritmo foi inicializar os vetores comando[j] e cmd[][] com zeros a fim de não imapctar o cálculo dos cossenos.
 Feito isso os comandos são carregados do arquivo dat e os espaços não preenchidos dos vetores já estão com zeros. De forma análoga para o vetor que recebe do teclado a palavra digitada. 

 Foi criador outro vetor para contar quantos comandos reconhecidos pelo usuário  a fim de fazer a estatística no final.
 Assim que o usuário digita a palavra, esta é carregada e calculado o cosseno entre tal palavra e cada comando. O comando correto será aquele cujo cosseno aproximar-se ou for igual 1 (digitado o comando exato). Após esse cálculo, o algoritimo teste se o valor é maior 0,4. Caso sim, algortimo reconhece que o usuário digitou algo parecido com um dos comandos internos e pergunta se realmente foi o que queria digitar. Caso não,  o algorítimo descarta a palavra digitada informando que a mesma é inválida.

 Relátorio: Foi feito um código baseado com o que nos foi apresentado com o Marcio fazendo principalmente o código e o Pedro e o João Victor ajudando e testando algumas coisas a partir do código proposto pelo Marcio. Infelizmente, não nos atentamos corretamente ao trabalho aonde deveria haver commits de todos os participantes, mas todos participaram na tentativa de atingir a proposta da atividade.

Link do Video: https://youtu.be/7L24kqurcME

 

