#include <stdio.h>
#include <math.h>
#include <unistd.h> 
#include <string.h>

#define ARQ_IN "commands.dat" //nome do arquivo de entrada com comandos e códigos
#define ARQ_OUT "saida.txt" //nome do arquivo de saida
#define num_comandos 8
#define comp_comandos 15

void clearScreen()
{
      const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J ";
      write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

int main()
{

    FILE *arquivo_in, *arquivo_out;

    char cmd [num_comandos][comp_comandos],comando[comp_comandos], linha, auxComando[comp_comandos],resp;
    int codigo[num_comandos], auxCodigo,i, j,l,contcomando[num_comandos],opcao=1;
    double vetCosseno[num_comandos], aux;
    double frase1=0,frase2=0, prodIntf1f2=0;

    clearScreen();

    arquivo_in = fopen(ARQ_IN,"r");
    arquivo_out = fopen(ARQ_OUT,"w");
    
    if( arquivo_in == NULL || arquivo_out == NULL)
    {
        printf("\n Arquivo %s ou %s não podem ser abertos \n\n", ARQ_IN, ARQ_OUT);
        return -1;
    }
    else
    {    
       
        for (j=0;j<comp_comandos;j++) //Inicializando o vetor comando[j] com zeros;
        {
         comando[j]=0;
        }
        printf("\n");
        
        
       for (i=0;i<num_comandos;i++)//preenchendo o vetor cmd com zeros
        {
          for(j=0;j<comp_comandos;j++)
            {
             cmd[i][j]=0;
             //printf("%d", cmd[i][j]);
            }   
           printf("\n"); 
        }
        //inicializa o contador de comandos digitados
        for (int k=0;k<num_comandos;k++)
        contcomando[k]=0;
               
       // printf("COMANDO  E  CÓDIGO CARREGADOS DO ARQUIVO:\n"); //Testando se os comandos e os código foram carregados do arquivo
        i=0;
        linha = fscanf(arquivo_in,"%s %x",cmd[i],&codigo[i]);
        while(linha != EOF )
        {  //printf("%s"   "%x \n",cmd[i],codigo[i]);            
            i++;
            linha = fscanf(arquivo_in,"%s %x", cmd[i],&codigo[i]); 
            printf("\n");
        }

         

      while(1)

        { 
                   
          printf("Entre com o comando:");
          scanf("%s",comando);
          printf("\n\n");
                 
          for (i=0;i<num_comandos;i++) //Calcula os cossenos de cada comando da lista com o comando digitado
          {                            // e armazena esses cossenos no vetor vetCosseno[num_comandos]
            for(j=0; j<comp_comandos;j++)
            {
              prodIntf1f2 = prodIntf1f2 + ((double)cmd[i][j]*(double)comando[j]);   
              frase1+= cmd[i][j]*cmd[i][j];
              frase2+= comando[j]*comando[j];          
            }
            vetCosseno[i]=(prodIntf1f2/(sqrt(frase1)*sqrt(frase2)));             
            printf("Comando %s   Cosseno[%d]= %.2f\n", cmd[i],i+1, vetCosseno[i]);
            frase1=0;
            frase2=0;
            prodIntf1f2=0; 
          }

// Testando qual comando cujo cosseno se aproxima de 1
aux=vetCosseno[0]; //assume vetCosseno[0] como o menor valor de cosseno
for (i=0;i<num_comandos;i++)
{
  if (aux <= vetCosseno[i])
    {
      aux = vetCosseno[i];
      auxCodigo=codigo[i];
      for (int k=0;k<comp_comandos;k++)           
           {             
             comando[k]=0;
             auxComando[k]=cmd[i][k]; //copia da matriz cmd[][] de comandos o comando correspondente ou próximo do dgitado
           }           
    }     
}
    if (aux>=0.4) //Se o cosseno for maior que 0.4, identifica o comando igual ou próximo ao da lista.
    {
      printf("\n");
      printf(" Você quis digitar o:  %s  -   Código: %x  \n", auxComando, auxCodigo);
      printf("É esse mesmo? Digite ('s ou S' para SIM ou outra letra para- Não)");
      __fpurge(stdin); //limpa o buffer do teclado.
      scanf("%c",&resp);
      // Pergunta  se o usuário escolheu o comando correto. Caso afirmativo, incrementa o contador daquele comando   
      if ((resp== 's') ||  (resp=='S'))
        {
          
         for (i=0;i<num_comandos;i++)
         {
           if (aux == vetCosseno[i])
           {
             contcomando[i]++;;
             l=i;
           } 
            
         }
         printf("Estatística do comando %s  %d",auxComando, contcomando[l]);
         printf("\n");   
         fprintf(ARQ_OUT,auxComando, contcomando[l]);  // escreve a estatítica de comandos digitados   
        
        }
          
    }
    
    else
     {       
       printf("O comando digitado não existe.\n"); // se a similaridade do cosseno for menos que 0.4 o comando digitado não é reconhecido
     }     

      for(int w=0;w<comp_comandos;w++)//limpando os vetores auxiliares auxComando[k],comando[k] e vetCosseno[k]
        {
         auxComando[w]=0;
         comando[w];
        // vetCosseno[w]=0;
        }                 
}  //fim do While
     
     fclose(arquivo_in); 
     fclose(arquivo_out);
       
    }

return 0;
}